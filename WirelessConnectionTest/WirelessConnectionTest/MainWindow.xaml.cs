﻿using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NativeWifi;
using System.Linq;

namespace WirelessConnectionTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public class AvailableWifi
        {
            public string SSID { get; set; }
            public string Signal { get; set; }
            public string Encryption { get; set; }
        }

        public MainWindow()
        {
            InitializeComponent();
            WlanClient client = new WlanClient();
            WlanClient.WlanInterface wlanInterface = client.Interfaces.Where(x => x.InterfaceState == Wlan.WlanInterfaceState.Connected).FirstOrDefault();
            if (wlanInterface != null)
                btnDisconnect.IsEnabled = true;
            else
                btnDisconnect.IsEnabled = false;

        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            WlanClient client = new WlanClient();
            List<AvailableWifi> availableWifiList = new List<AvailableWifi>();
            foreach (WlanClient.WlanInterface wlanInterface in client.Interfaces)
            {
                Wlan.WlanAvailableNetwork[] networks = wlanInterface.GetAvailableNetworkList(0);
                foreach (Wlan.WlanAvailableNetwork network in networks)
                {
                    Wlan.Dot11Ssid ssid = network.dot11Ssid;
                    string networkName = Encoding.ASCII.GetString(ssid.SSID, 0, (int)ssid.SSIDLength);
                    availableWifiList.Add(new AvailableWifi() { SSID = networkName, Signal = network.wlanSignalQuality.ToString() + "%", Encryption = network.dot11DefaultCipherAlgorithm.ToString() });
                }
            }
            lstNetworks.ItemsSource = availableWifiList;
        }

        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            Connect();
            btnDisconnect.IsEnabled = true;
        }

        private void btnDisconnect_Click(object sender, RoutedEventArgs e)
        {
            Disconnect();
            btnDisconnect.IsEnabled = false;
        }

        private void Connect()
        {
            WlanClient client = new WlanClient();
            foreach (WlanClient.WlanInterface wlanInterface in client.Interfaces)
            {
                foreach (Wlan.WlanProfileInfo profileInfo in wlanInterface.GetProfiles())
                {
                    if (profileInfo.profileName == lblSelectedSSID.Content.ToString())
                    {
                        string name = profileInfo.profileName;
                        string key = txtPwd.Password;
                        string profileXml = wlanInterface.GetProfileXml(name);
                        wlanInterface.SetProfile(Wlan.WlanProfileFlags.AllUser, profileXml, true);
                        wlanInterface.Connect(Wlan.WlanConnectionMode.Profile, Wlan.Dot11BssType.Any, name);
                    }
                }
            }
        }

        private void Disconnect()
        {
            WlanClient client = new WlanClient();
            WlanClient.WlanInterface wlanInterface = client.Interfaces.Where(x => x.InterfaceState == Wlan.WlanInterfaceState.Connected).FirstOrDefault();
            if (wlanInterface != null)
                wlanInterface.DeleteProfile(lblSelectedSSID.Content.ToString());
        }

        private void lstNetworks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (txtPwd.Password.Length > 0) txtPwd.Clear();
        }
    }
}
